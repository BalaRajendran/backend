docker rm -f postgres
docker run -d --restart unless-stopped --name postgres -p 5433:5432 -e "POSTGRES_USER=user" -e "PG_PASSWORD=password" -e "POSTGRES_HOST_AUTH_METHOD=trust" -e "POSTGRES_DB=delimile-local" postgres:9.6
sleep 5s
npm run prisma:migrate
npm run prisma:seed