export class CommonConstants {
  static SUCCESS = 'SUCCESS';
  static ERROR = 'ERROR';

  static ERROR_CODE = {
    NOT_FOUND: 404,
    INTERNAL_SERVER_ERROR: 500,
    BAD_REQUEST: 400,
    UN_PROCESSED_REQUEST: 422,
  };

  static BRANCH_TYPE = {
    ALL: 'ALL',
    INDIVIDUAL: 'INDIVIDUAL',
  };

  static SUCCESS_CODE = {
    OK: 200,
    CREATED: 201,
  };

  static allowedMediaExtension = [
    'jpeg',
    'jpg',
    'png',
    'docx',
    'xlsx',
    'csv',
    'ppt',
    'pdf',
  ];
}
