import { Prisma } from '@prisma/client';

export const prismaErrorHandler = (error: any) => {
  let errorResponse = {};
  if (error instanceof Prisma.PrismaClientKnownRequestError) {
    if (error.code === 'P2002') {
      errorResponse = {
        message: error.message,
        name: error.name,
        meta: error.meta,
      };
    }
  }

  return null;
};

export const stringSlugify = (...args: string[]): string => {
  const value = args.join(' ');

  return value
    .normalize('NFD') // split an accented letter in the base letter and the acent
    .replace(/[\u0300-\u036f]/g, '') // remove all previously split accents
    .toLowerCase()
    .trim()
    .replace(/[^a-z0-9 ]/g, '') // remove all chars not letters, numbers and spaces (to be replaced)
    .replace(/\s+/g, '-'); // separator
};

export const getPagination = (page: number, size: number) => {
  page = +page - 1;
  const limit = size ? +size : 10;
  const offset = page ? +page * limit : 0;
  return { limit, offset };
};
