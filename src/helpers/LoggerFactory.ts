import * as winston from 'winston';
import path = require('path');

import dailyRotateFile = require('winston-daily-rotate-file');

import config = require('config');
import { FileTransportOptions } from 'winston/lib/winston/transports';

const { combine, timestamp, printf, splat, label, metadata } = winston.format;

export default class LoggerFactory {
  static getLogger(callingModule): winston.Logger {
    return LoggerFactory.init(callingModule);
  }

  static getLabel(callingModule): string {
    const parts = callingModule.filename.split(path.sep);
    return path.join(parts[parts.length - 2], parts.pop());
  }

  // eslint-disable-next-line object-curly-newline
  static customFileFormatter = printf(({ level, message, timestamp, meta }) => {
    const outputMeta = meta ? `||${JSON.stringify(meta)}` : '';
    return `${timestamp} : ${level.toUpperCase()}: ${message} ${outputMeta}`;
  });

  static logFormat = printf(
    (info) =>
      `${info.timestamp} : ${info.level} [${info.label}]: ${info.message}`
  );

  static fileOptions: FileTransportOptions = {
    level: config.get('LOG.File.Level'),
    dirname: config.get('LOG.File.Dir'),
    filename: config.get('LOG.File.FileName'),
    handleExceptions: true,
  };

  static consoleOptions = { level: 'debug', timestamp: true };

  static init(callingModule): winston.Logger {
    const logger = winston.createLogger({
      level: process.env.NODE_ENV === 'production' ? 'info' : 'debug',
      format: combine(
        label({ label: LoggerFactory.getLabel(callingModule) }),
        timestamp(),
        splat(),
        // colorize(),
        metadata({ fillExcept: ['message', 'level', 'timestamp', 'label'] }),
        // this.customFileFormatter,
        this.logFormat
      ),
      defaultMeta: { service: 'bot-service' },
      transports: [
        new dailyRotateFile({
          filename: 'colorshadesErpForms_out_%DATE%.log',
          datePattern: 'YYYY-MM-DD',
          dirname: config.get('LOG.File.Dir'),
          maxSize: '100m',
          maxFiles: '7d',
        }),
        new dailyRotateFile({
          filename: 'colorshadesErpForms_out__error_%DATE%.log',
          dirname: config.get('LOG.File.Dir'),
          datePattern: 'YYYY-MM-DD',
          level: 'error',
          maxSize: '40m',
          maxFiles: '14d',
        }),
      ],
    });

    if (config.get('LOG.Transports.File') === 'Enable') {
      logger.add(new winston.transports.File(this.fileOptions));
    }

    if (config.get('LOG.Transports.Console') === 'Enable') {
      logger.add(new winston.transports.Console(this.consoleOptions));
    }
    return logger;
  }
}
