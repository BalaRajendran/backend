export class StatusMessages {
  static newVendorCreated = 'New Vendor has been created successfully';
  static newCustomerCreated = 'New Customer has been created successfully';
  static newBranchCreated = 'New Branch has been created successfully';
  static imageUploaded = 'Image uploaded successfully';
  static newUserCreated = 'New User has been created successfully';
  static newOrderCreated = 'Order placed successfully';
  static getOrders = 'Orders retrieved successfully';
  static assignedDE = 'DE assigned successfully';
  static updatedDE = 'DE updated successfully';
  static getMetadata = 'Metadata retrieved successfully';
  static getVendor = 'Vendor retrieved successfully';
  static fetchRecordsSuccess = 'Records retrieved successfully';
  static getUsers = 'Users retrieved successfully';
  static loginVendor = 'Login successfully';
  static deleteVendor = 'Vendor deleted successfully';
  static updateVendorCreated = 'Vendor updated successfully';
  static phoneNumberExist = 'Oops! Phone Number already exist';
  static orderExist = 'Oops! Order already exist';
  static emailExist = 'Oops! Email already exist';
  static accountNotFound = 'Oops! Account not found';
  static invalidPassword = 'Oops! Invalid Password';

  static wentWrong = 'Oops! Something went wrong';
  static badRequest = 'Oops! Seems like bad request';
  static retrievedSuccess = 'Records retrieved successfully';
  static parameterMission = 'Parameter is Invalid/Missing';
}
