import { KeyValuePair } from '../models/KeyValuePair';
import LoggerFactory from './LoggerFactory';

let logger = LoggerFactory.getLogger(module);

export class ObjectUtil {
  public static escapeString(input: string): string {
    logger.info(`about to escape string ${input}`);
    const output: string = input.replace(/\n/g, '\\n\\n').replace(/\r/g, '\\r');
    logger.info(`escapedString: ${output}`);
    return output;
  }

  public static escapeAllPropertiesInObject(obj: any): any {
    const escapedObject: any = {};
    let escapedValue: string;
    for (const prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        if (typeof obj[prop] === 'string') {
          escapedValue = ObjectUtil.escapeString(obj[prop]);
        } else {
          escapedValue = ObjectUtil.escapeAllPropertiesInObject(obj[prop]);
        }
        escapedObject[prop] = escapedValue;
      }
    }
    return escapedObject;
  }

  public static createObjectFromMap(map: Map<string, any>): any {
    let object: any = undefined;
    logger.info(`inputMap: ${map}`);
    if (map && map.size > 0) {
      object = {};
      map.forEach((value, key) => {
        object[key] = value;
      });
      logger.info(`objectFromMap: ${JSON.stringify(object)}`);
    } else {
      logger.info(`map passed is empty.. returning undefined`);
    }
    return object;
  }

  public static createObjectFromKeyValuePairArray(
    keyValuePairs: Array<KeyValuePair>
  ): any {
    let object: any = undefined;
    logger.info(`keyValuePairs: ${keyValuePairs}`);
    if (keyValuePairs && keyValuePairs.length > 0) {
      object = {};
      keyValuePairs.forEach((keyValuePair) => {
        object[keyValuePair.key] = keyValuePair.value;
      });
      logger.info(`objectFromMap: `, object);
    } else {
      logger.info(`array passed is empty.. returning undefined`);
    }
    return object;
  }

  static isAttributeEmpty(obj: any, attribute: string): boolean {
    let attrValue: any;
    if (ObjectUtil.isNotNullAndUndefined(obj)) {
      logger.info(`object is not null`);
      attrValue = obj[attribute];
      if (typeof attrValue === 'string') {
        logger.info(`attrValue is of type string`);
        return !(attrValue.length > 0);
      } else {
        logger.info(`returning false as attrValue is not of type string`);
      }
    } else {
      logger.info('object is null');
    }
    return true;
  }

  static isNotNullAndUndefined(obj: any): boolean {
    if (obj && obj !== null) {
      return true;
    }
    return false;
  }

  static isNullOrUndefinedOrEmpty(obj: any): boolean {
    let resp = false;
    if (obj === undefined || obj === null) {
      resp = true;
    } else {
      if (typeof obj === 'string' && obj.trim().length === 0) {
        resp = true;
      } else if (Array.isArray(obj) && obj.length === 0) {
        resp = true;
      } else if (typeof obj === 'object' && Object.keys(obj).length === 0) {
        resp = true;
      }
    }
    return resp;
  }

  static isNotEmpty(obj: any): boolean {
    return !ObjectUtil.isNullOrUndefinedOrEmpty(obj);
  }
}
