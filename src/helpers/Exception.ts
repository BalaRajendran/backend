export class Exception extends Error {
  displayMessage: string;
  errorCode: string;
  constructor(displayMessage, errorCode, ...params) {
    // Pass remaining arguments (including vendor specific ones) to parent constructor
    super(...params);

    // Maintains proper stack trace for where our error was thrown (only available on V8)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, Exception);
    }

    this.name = 'Exception';
    // Custom debugging information
    this.message = displayMessage;
    this.displayMessage = displayMessage;
    this.errorCode = errorCode;
  }
}
