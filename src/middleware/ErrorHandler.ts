import { NextFunction, Request, Response } from 'express';

class ErrorHandler {
  static handle(
    err: Error,
    request: Request,
    response: Response,
    next: NextFunction
  ): Response {
    if (err instanceof Error) {
      return response.status(400).json({
        message: err.message,
      });
    }

    return response.status(500).json({
      status: 'error',
      message: `Internal server error - ${err}`,
    });
  }
}

export { ErrorHandler };
