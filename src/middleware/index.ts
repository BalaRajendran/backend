import LoggerFactory from '../helpers/LoggerFactory';

const logger = LoggerFactory.getLogger(module);

export const requestMiddleware = (req, res, next) => {
  req.requestTime = Date.now();

  // headers
  req.body.headers = {
    vendorId: req.headers['vendor-id'],
    userId: +req.headers['user-id'],
    accessType: req.headers['access-type'],
    userName: req.headers['user-name'],
    branchId:
      req.headers['branch-id'] === 'ALL' ? 'ALL' : +req.headers['branch-id'],
  };
  req.requestTime = Date.now();
  logger.info('Got request from client ' + Date.now());
  next();
};

export const responseMiddleware = (req, res, next) => {
  logger.info('Got request from client ' + (Date.now() - req.requestTime));
  next();
};
