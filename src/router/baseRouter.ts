import { BaseController } from '../controller/BaseController';

const router = require('express').Router();

router.get('/health', new BaseController().healthCheck);
router.get('/metadata', new BaseController().metadata);
router.get('/address/:pincode', new BaseController().getAddressByPincode);

module.exports = router;
