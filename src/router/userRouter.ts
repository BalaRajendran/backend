import { UserController } from '../controller/UserController';

const router = require('express').Router();

router.post('/user', new UserController().createUser);
router.post('/get/user', new UserController().getUsers);
router.post(
  '/get/user/deliveryExecutive',
  new UserController().availableDeliveryExecutives
);

module.exports = router;
