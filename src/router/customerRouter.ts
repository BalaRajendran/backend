import { CustomerController } from '../controller/CustomerController';

const router = require('express').Router();

router.get(
  '/customer/:phoneNumber',
  new CustomerController().getCustomerAddressByPhoneNumber
);
router.post('/customer', new CustomerController().createCustomer);
router.post('/get/customers', new CustomerController().getCustomers);

module.exports = router;
