import { OrderController } from '../controller/OrderController';
import { OrderStatusController } from '../controller/OrderStatusController';

const router = require('express').Router();

router.post('/order', new OrderController().createOrder);
router.post('/get/order', new OrderController().getOrderByVendorId);
router.post('/assignDE', new OrderController().assignDE);
router.put('/assignDE', new OrderController().updateDE);
router.post('/get/order/:id', new OrderController().getOrderByOrderId);

router.post('/get/order-history', new OrderStatusController().getTrackHistory);
router.post(
  '/update/order/status',
  new OrderStatusController().updateOrderStatus
);

router.post(
  '/de/status',
  new OrderStatusController().getDECurrentStatusByVendorId
);

module.exports = router;
