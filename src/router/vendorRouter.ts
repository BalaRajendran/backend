import { VendorController } from '../controller/VendorController';
import { LoginController } from '../controller/LoginController';

const router = require('express').Router();

router.get('/vendor/:id', new VendorController().getVendorById);
router.post('/vendor', new VendorController().createVendor);
router.put('/vendor', new VendorController().updateVendor);
router.delete('/vendor/:id', new VendorController().deleteVendorById);

router.post('/vendor/login', new LoginController().vendorLogin);

module.exports = router;
