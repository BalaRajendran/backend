import { BranchController } from '../controller/BranchController';

const router = require('express').Router();

router.post('/branch', new BranchController().createBranch);

module.exports = router;
