const { Eureka } = require('eureka-js-client');
const config = require('config');

const eurekaHost = config.get('EUREKA.host');
const eurekaPort = config.get('EUREKA.port');
const serviceHost = config.get('SERVER.serverHost');
const servicePort = config.get('SERVER.port');
const serviceName = config.get('SERVER.serviceName');

export default class EurekaHelperService {
  static registerWithEureka = function () {
    const configEureka = {
      instance: {
        app: serviceName,
        hostName: serviceHost,
        ipAddr: serviceHost,
        vipAddress: serviceName,
        instanceId: `${serviceHost}:${servicePort}`,
        port: {
          $: servicePort,
          '@enabled': 'true'
        },
        dataCenterInfo: {
          '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
          name: 'MyOwn'
        }
      },
      eureka: {
        host: eurekaHost,
        port: eurekaPort,
        maxRetries: 25,
        servicePath: '/eureka/apps/'
      }
    };
    const client = new Eureka(configEureka);

    client.logger.level('debug');

    client.start((error) => {
      console.log(error || 'Hey! Service registered with Eureka');
    });

    client.on('deregistered', () => {
      console.log('Hey! Service deregistered with Eureka');
      process.exit();
    });

    const exitHandler = (options, exitCode) => {
      if (exitCode || exitCode === 0) console.log(exitCode);
      if (options.exit) {
        client.stop();
      }
    };

    process.on('SIGINT', exitHandler.bind(null, { exit: true }));
  };
}
