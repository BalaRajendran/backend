import { PrismaClient } from '@prisma/client';
import LoggerFactory from '../helpers/LoggerFactory';

let logger = LoggerFactory.getLogger(module);

export class PrismaConnectionService {
  static connection: PrismaClient<
    { log: { emit: 'event'; level: 'query' }[] },
    'query',
    false
  >;
  constructor() {}
  static getDBConnection(): PrismaClient<
    { log: { emit: 'event'; level: 'query' }[] },
    'query',
    false
  > {
    let connection = new PrismaClient({
      log: [
        {
          emit: 'event',
          level: 'query',
        },
      ],
    });

    connection.$on('query', (e) => {
      console.log('Query: ' + e.query);
      console.log('Params: ' + e.params);
      console.log('Duration: ' + e.duration + 'ms');
    });

    this.connection = connection;
    return connection;
  }
}
