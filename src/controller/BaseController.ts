import { Request, Response, NextFunction } from 'express';
import { CommonConstants } from '../helpers/CommonConstants';
import LoggerFactory from '../helpers/LoggerFactory';
import { metadata } from '../helpers/metadata';
import { StatusMessages } from '../helpers/StatusMessages';
import { ObjectUtil } from '../helpers/ObjectUtil';
import { PrismaConnectionService } from '../service/PrismaConnectionService';

let logger = LoggerFactory.getLogger(module);

export class BaseController {
  healthCheck(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info('Health check ping');
      res.status(200).send({ status: 'Service health is Good' });
    } catch (error) {
      logger.error(`Error trace back -> ${error}`);
      res.status(500).send({ status: 'ERROR' });
    }
  }

  metadata(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info('About to get metadata');
      res.status(CommonConstants.SUCCESS_CODE.CREATED).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.getMetadata,
        data: metadata,
      });
    } catch (error) {
      logger.error(`Error trace back -> ${error}`);
      res
        .status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR)
        .send({ status: CommonConstants.ERROR, data: {} });
    }
  }

  async getAddressByPincode(req: Request, res: Response, next: NextFunction) {
    try {
      const pincode = req.params.pincode;
      logger.info('About to get address by pincode ===>' + pincode);
      if (ObjectUtil.isNullOrUndefinedOrEmpty(pincode)) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      const db = PrismaConnectionService.getDBConnection();
      let pincodeResponse = await db.pincode.findMany({
        where: {
          pincode,
        },
      });

      if (pincodeResponse && pincodeResponse.length <= 0) {
        res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.wentWrong,
          data: [],
        });
        return next();
      }

      res.status(CommonConstants.SUCCESS_CODE.OK).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.retrievedSuccess,
        data: pincodeResponse[0],
      });
    } catch (error) {
      logger.error(`Error trace back -> ${error}`);
      res
        .status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR)
        .send({ status: CommonConstants.ERROR, data: {} });
    }
  }
}
