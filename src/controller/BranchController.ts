import { Request, Response, NextFunction } from 'express';
import { CommonConstants } from '../helpers/CommonConstants';
import { Prisma } from '@prisma/client';

import LoggerFactory from '../helpers/LoggerFactory';
import { ObjectUtil } from '../helpers/ObjectUtil';
import { StatusMessages } from '../helpers/StatusMessages';
import { PrismaConnectionService } from '../service/PrismaConnectionService';
import { UserController } from './UserController';

let logger = LoggerFactory.getLogger(module);

export class BranchController {
  async createBranch(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info(`About to insert new branch ${JSON.stringify(req.body)}`);
      const requestBody = req.body;

      if (
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.branchName) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.password) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.email) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.address1) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.city) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.pincode) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.managerName) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.state) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.phoneNumber1) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.pickupStartsFrom) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.pickupCloseAt) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.closeOn) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.weekOffDays) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.lat) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.long)
      ) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      const db = PrismaConnectionService.getDBConnection();

      const checkIfBusinessExist = await db.vendor.findFirst({
        where: {
          OR: [
            {
              phoneNumber: requestBody.phoneNumber1,
            },
            { email: requestBody.email },
          ],
          AND: [
            {
              id: requestBody.headers.vendorId,
            },
          ],
        },
      });

      if (ObjectUtil.isNotNullAndUndefined(checkIfBusinessExist)) {
        let validationError = {};
        if (checkIfBusinessExist.phoneNumber === requestBody.phoneNumber1) {
          validationError['phoneNumber'] = StatusMessages.phoneNumberExist;
        }

        if (checkIfBusinessExist.email === requestBody.email) {
          validationError['email'] = StatusMessages.emailExist;
        }

        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.wentWrong,
          data: validationError,
        });
        return next();
      }

      const checkIfBranchExist = await db.branch.findFirst({
        where: {
          OR: [
            {
              phoneNumber1: requestBody.phoneNumber1,
            },
            { email: requestBody.email },
          ],
          AND: [
            {
              vendorId: requestBody.headers.vendorId,
            },
          ],
        },
      });

      if (ObjectUtil.isNotNullAndUndefined(checkIfBranchExist)) {
        let validationError = {};
        if (checkIfBranchExist.phoneNumber1 === requestBody.phoneNumber1) {
          validationError['phoneNumber'] = StatusMessages.phoneNumberExist;
        }

        if (checkIfBranchExist.email === requestBody.email) {
          validationError['email'] = StatusMessages.emailExist;
        }

        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.wentWrong,
          data: validationError,
        });
        return next();
      }

      const checkIfPhoneNumberExistAsUser = await db.user.findFirst({
        where: {
          OR: [
            { email: requestBody.email },
            {
              phoneNumber: requestBody.phoneNumber,
            },
          ],
          AND: [
            {
              vendorId: requestBody.headers.vendorId,
            },
          ],
        },
      });

      if (ObjectUtil.isNotNullAndUndefined(checkIfPhoneNumberExistAsUser)) {
        let validationError = {};
        if (
          checkIfPhoneNumberExistAsUser.phoneNumber === requestBody.phoneNumber1
        ) {
          validationError['phoneNumber'] = StatusMessages.phoneNumberExist;
        }

        if (checkIfPhoneNumberExistAsUser.email === requestBody.email) {
          validationError['email'] = StatusMessages.emailExist;
        }

        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.wentWrong,
          data: validationError,
        });
        return next();
      }

      let user: Prisma.branchCreateInput = {
        branchName: requestBody.branchName,
        branchType: requestBody.branchType,
        email: requestBody.email,
        address1: requestBody.address1,
        address2: requestBody.address2,
        pincode: requestBody.pincode,
        landmark: requestBody.landmark,
        city: requestBody.city,
        state: requestBody.state,
        country: requestBody.country,
        phoneNumber1: requestBody.phoneNumber1,
        phoneNumber2: requestBody.phoneNumber2,
        pickupStartsFrom: requestBody.pickupStartsFrom,
        pickupCloseAt: requestBody.pickupCloseAt,
        startsFrom: requestBody.startsFrom,
        closeOn: requestBody.closeOn,
        weekOffDays: requestBody.weekOffDays,
        lat: requestBody.lat,
        long: requestBody.long,
        vendor: {
          connect: {
            id: requestBody.headers.vendorId,
          },
        },
      };
      const branchDetails = await db.branch.create({
        data: user,
        select: {
          id: true,
          email: true,
          address1: true,
          address2: true,
          pincode: true,
          city: true,
          state: true,
          country: true,
          phoneNumber1: true,
          phoneNumber2: true,
          pickupStartsFrom: true,
          pickupCloseAt: true,
          startsFrom: true,
          closeOn: true,
          weekOffDays: true,
          lat: true,
          long: true,
        },
      });

      await new UserController().createUserFromRequest({
        userName: requestBody.managerName,
        roleId: 1,
        assignedBranch: [branchDetails.id],
        password: requestBody.password,
        email: requestBody.email,
        status: 'ACTIVE',
        accessType: 'INDIVIDUAL',
        phoneNumber: requestBody.phoneNumber1,
        headers: requestBody.headers,
      });

      res.status(CommonConstants.SUCCESS_CODE.CREATED).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.newBranchCreated,
        data: branchDetails,
      });
    } catch (error) {
      console.error(error);
      logger.error(`Got error while inserting new vendor -> ${error.message}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }
}
