import { Request, Response, NextFunction } from 'express';
import LoggerFactory from '../helpers/LoggerFactory';
import { ObjectUtil } from '../helpers/ObjectUtil';
import { CommonConstants } from '../helpers/CommonConstants';
import { StatusMessages } from '../helpers/StatusMessages';
import { PrismaConnectionService } from '../service/PrismaConnectionService';
import { getPagination } from '../helpers/helperFunctions';

let logger = LoggerFactory.getLogger(module);

export class OrderStatusController {
  async updateOrderStatus(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info(`About to update status ${JSON.stringify(req.body)}`);
      const requestBody = req.body;

      if (
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.orderId) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.status)
      ) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      const db = PrismaConnectionService.getDBConnection();

      const orderDetails = await db.order.findFirst({
        where: {
          id: +requestBody.orderId,
          vendorId: requestBody.headers.vendorId,
        },
        select: {
          id: true,
          deliverySlotFrom: true,
          deliverySlotTo: true,
          deliveryDate: true,
          status: true,
          assignedOrderDetailsToDE: {
            where: {
              orderId: +requestBody.orderId,
            },
            select: {
              id: true,
            },
          },
        },
      });

      let trackStatusMessage = '';

      let orderUpdated = false;

      if (
        ObjectUtil.isNullOrUndefinedOrEmpty(orderDetails) ||
        orderDetails.status === requestBody.status
      ) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.wentWrong,
          data: {},
        });
        return next();
      }

      if (
        (orderDetails.status === 'CREATED' &&
          requestBody.status !== 'CANCELED') ||
        orderDetails.status === 'CANCELED' ||
        orderDetails.status === 'RETURNED'
      ) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.wentWrong,
          data: {
            status: "Don't updated created order",
          },
        });
        return next();
      }

      if (
        orderDetails.status === 'CREATED' &&
        requestBody.status === 'CANCELED'
      ) {
        trackStatusMessage = `<strong>${requestBody.headers.userName}(${requestBody.headers.userId})</strong> updated status to <strong>Canceled</strong>`;
        await db.order.update({
          where: {
            id: orderDetails.id,
          },
          data: {
            status: 'CANCELED',
          },
        });
        await db.orderDeliveryDetails.update({
          where: {
            orderId: orderDetails.id,
          },
          data: {
            cancelledOrReturnReason: requestBody?.metadata?.reason,
          },
        });
        orderUpdated = true;
      } else if (
        requestBody.status === 'CREATED' ||
        requestBody.status === 'CANCELED'
      ) {
        if (requestBody.status === 'CREATED') {
          trackStatusMessage = `<strong>${requestBody.headers.userName}(${requestBody.headers.userId})</strong> updated status to <strong>Created</strong>`;
        } else if (requestBody.status === 'CANCELED') {
          trackStatusMessage = `<strong>${requestBody.headers.userName}(${requestBody.headers.userId})</strong> updated status to <strong>Canceled</strong>`;
        }

        await db.assignedOrderDetailsToDE.delete({
          where: {
            orderId: orderDetails.id,
          },
        });

        await db.orderDeliveryDetails.update({
          where: {
            orderId: orderDetails.id,
          },
          data: {
            deliveryExecutiveId: null,
            cancelledOrReturnReason: requestBody?.metadata?.reason,
          },
        });

        await db.order.update({
          where: {
            id: orderDetails.id,
          },
          data: {
            status: requestBody.status,
          },
        });
        orderUpdated = true;
      } else if (
        orderDetails.status === 'ASSIGNED_DE' &&
        requestBody.status === 'DE_ACCEPTED'
      ) {
        trackStatusMessage = `<strong>${requestBody.headers.userName}(${requestBody.headers.userId})</strong> updated status to <strong>Accepted</strong>`;
        await db.order.update({
          where: {
            id: orderDetails.id,
          },
          data: {
            status: requestBody.status,
          },
        });
        orderUpdated = true;
      } else if (
        orderDetails.status === 'DE_ACCEPTED' &&
        requestBody.status === 'OUT_FOR_DELIVERY'
      ) {
        trackStatusMessage = `<strong>${requestBody.headers.userName}(${requestBody.headers.userId})</strong> updated status to <strong>Out for delivery</strong>`;
        await db.order.update({
          where: {
            id: orderDetails.id,
          },
          data: {
            status: requestBody.status,
          },
        });
        await db.orderDeliveryDetails.update({
          where: {
            orderId: orderDetails.id,
          },
          data: {
            deliveryPickUpTime: new Date(),
          },
        });
        orderUpdated = true;
      } else if (
        orderDetails.status === 'DE_ACCEPTED' &&
        requestBody.status === 'ASSIGNED_DE'
      ) {
        trackStatusMessage = `<strong>${requestBody.headers.userName}(${requestBody.headers.userId})</strong> updated status to <strong>Assigned orders</strong>`;
        await db.order.update({
          where: {
            id: orderDetails.id,
          },
          data: {
            status: requestBody.status,
          },
        });
        orderUpdated = true;
      } else if (
        orderDetails.status === 'OUT_FOR_DELIVERY' &&
        (requestBody.status === 'DELIVERED' ||
          requestBody.status === 'DAMAGED' ||
          requestBody.status === 'RETURNED')
      ) {
        let data: any = {};
        let deliveryExecutiveMetaQuery: any = {};

        if (requestBody.status === 'DELIVERED') {
          trackStatusMessage = `<strong>${requestBody.headers.userName}(${requestBody.headers.userId})</strong> updated status to <strong>delivered</strong>`;
          data = {
            cancelledOrReturnReason: requestBody?.metadata?.reason,
            collectionAmount: +requestBody?.metadata?.collectionAmount,
            deliveredTime: new Date(),
          };
          deliveryExecutiveMetaQuery = {
            noOfSuccessfullDeliveries: {
              increment: 1,
            },
          };
        } else if (requestBody.status === 'DAMAGED') {
          trackStatusMessage = `<strong>${requestBody.headers.userName}(${requestBody.headers.userId})</strong> updated status to <strong>Damaged</strong>`;
          data = {
            deliveryExecutiveId: null,
            cancelledOrReturnReason: requestBody?.metadata?.reason,
          };
          deliveryExecutiveMetaQuery = {
            noOfCanceledDeliveries: {
              increment: 1,
            },
          };
        } else if (requestBody.status === 'RETURNED') {
          trackStatusMessage = `<strong>${requestBody.headers.userName}(${requestBody.headers.userId})</strong> updated status to <strong>Returned</strong>`;
          data = {
            deliveryExecutiveId: null,
            cancelledOrReturnReason: requestBody?.metadata?.reason,
          };
          deliveryExecutiveMetaQuery = {
            noOfCanceledDeliveries: {
              increment: 1,
            },
          };
        }
        const deliveryExecutive = await db.orderDeliveryDetails.findFirst({
          where: {
            orderId: orderDetails.id,
          },
          select: {
            deliveryExecutiveId: true,
          },
        });
        await db.assignedOrderDetailsToDE.delete({
          where: {
            orderId: orderDetails.id,
          },
        });

        await db.deliveryUserMetadata.update({
          where: {
            userId: deliveryExecutive.deliveryExecutiveId,
          },
          data: deliveryExecutiveMetaQuery,
        });

        if (data) {
          await db.orderDeliveryDetails.update({
            where: {
              orderId: orderDetails.id,
            },
            data,
          });
        }

        await db.order.update({
          where: {
            id: orderDetails.id,
          },
          data: {
            status: requestBody.status,
          },
        });
        orderUpdated = true;
      }

      if (orderUpdated) {
        await new OrderStatusController().updateOrderHistory({
          branchId: requestBody.headers.branchId,
          vendorId: requestBody.headers.vendorId,
          orderId: orderDetails.id,
          remarks: trackStatusMessage,
          status: requestBody.status,
          createdBy: requestBody.headers.userName,
        });
        res.status(CommonConstants.SUCCESS_CODE.CREATED).send({
          status: CommonConstants.SUCCESS,
          message: StatusMessages.updatedDE,
        });
      } else {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.wentWrong,
        });
      }
    } catch (error) {
      logger.error(`Error trace back -> ${error}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }

  async updateOrderHistory(request: {
    remarks: string;
    status: string;
    vendorId: string;
    branchId: any;
    orderId: number;
    createdBy: string;
  }) {
    try {
      logger.info(`About to insert new order ${JSON.stringify(request)}`);

      if (
        ObjectUtil.isNullOrUndefinedOrEmpty(request.remarks) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(request.status) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(request.vendorId) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(request.branchId)
      ) {
        logger.error(`Validation failed`);
        return { isCreated: false };
      }

      const db = PrismaConnectionService.getDBConnection();

      let query: any = {
        data: {
          remarks: request.remarks,
          status: request.status as any,
          createdBy: request.createdBy,
          order: {
            connect: {
              id: request.orderId,
            },
          },
          vendor: {
            connect: {
              id: request.vendorId,
            },
          },
        },
      };

      if (request.branchId !== 'ALL') {
        query = {
          data: {
            remarks: request.remarks,
            status: request.status as any,
            createdBy: request.createdBy,
            order: {
              connect: {
                id: request.orderId,
              },
            },
            branch: {
              connect: {
                id: request.branchId,
              },
            },
            vendor: {
              connect: {
                id: request.vendorId,
              },
            },
          },
        };
      }

      await db.track_order.create(query);
      return { isCreated: true };
    } catch (error) {
      console.error(error);
      logger.error(
        `Got error while inserting new track order -> ${error.message}`
      );
      return { isCreated: false };
    }
  }

  async getTrackHistory(req: Request, res: Response, next: NextFunction) {
    try {
      if (!req.body.filter || !req.body.filter.orderId) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }
      logger.info(`About to get users`);
      let sortOrder = { id: 'asc' };

      const requestBody = req.body;
      const { pageNo, pageSize, search } = requestBody;
      const { limit, offset } = getPagination(+pageNo, +pageSize);
      if (requestBody.sortOrder) {
        sortOrder = requestBody.sortOrder;
      }
      const db = PrismaConnectionService.getDBConnection();

      const searchableFields = [];
      const searchQuery = [];

      if (search) {
        searchableFields.forEach((field) => {
          searchQuery.push({
            [field]: { contains: search, mode: 'insensitive' },
          });
        });
      }

      let query: any = {
        skip: offset,
        take: limit,
        orderBy: sortOrder as any,
        where: {
          vendorId: requestBody.headers.vendorId,
          orderId: req.body.filter.orderId,
        },
      };

      query.select = {
        remarks: true,
        createdBy: true,
        createdAt: true,
        status: true,
      };
      const orderHistory = await db.track_order.findMany(query);

      let countQuery = query;
      delete countQuery.skip;
      delete countQuery.select;
      delete countQuery.take;

      const orderHistoryCount = await db.track_order.count(query);

      res.status(CommonConstants.SUCCESS_CODE.OK).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.retrievedSuccess,
        data: { records: orderHistory, totalCount: orderHistoryCount },
      });
    } catch (error) {
      console.error(error);
      logger.error(`Got error while inserting new vendor -> ${error.message}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }

  async getDECurrentStatusByVendorId(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      logger.info(`About to get available DE`);
      let sortOrder = { id: 'asc' };
      const requestBody = req.body;
      const { pageNo, pageSize, search, filter } = requestBody;
      const { limit, offset } = getPagination(+pageNo, +pageSize);

      if (requestBody.sortOrder) {
        sortOrder = requestBody.sortOrder;
      }

      const db = PrismaConnectionService.getDBConnection();
      const deRole = 3;

      const LAT_LONG = [
        {
          lat: 12.92087,
          long: 80.16792,
        },
        {
          lat: 12.95087,
          long: 80.17792,
        },
        {
          lat: 12.97681,
          long: 80.22149,
        },
        {
          lat: 12.973984,
          long: 80.220343,
        },
      ];

      let branchId =
        requestBody.headers.accessType === 'ALL'
          ? filter.branch === null
            ? 'ALL'
            : filter.branch
          : requestBody.headers.branchId;
      let query: any = null;
      if (branchId === CommonConstants.BRANCH_TYPE.ALL) {
        query = {
          select: {
            id: true,
            name: true,
            phoneNumber: true,
          },
          where: {
            AND: [
              { vendorId: requestBody.headers.vendorId },
              { roleId: deRole },
              { status: 'ACTIVE' },
            ],
          },
        };
      } else {
        query = {
          select: {
            id: true,
            name: true,
            phoneNumber: true,
          },
          where: {
            AND: [
              { vendorId: requestBody.headers.vendorId },
              {
                assignedUserBranch: {
                  every: {
                    branchId: filter.branchId,
                  },
                },
              },
              { roleId: deRole },
              { status: 'ACTIVE' },
            ],
          },
        };
      }
      const deliveryExecutives = await db.user.findMany({
        select: {
          id: true,
          name: true,
          phoneNumber: true,
          assignedUserBranch: {
            select: {
              branchId: true,
              branch: {
                select: {
                  branchName: true,
                },
              },
            },
          },
          deliveryUserMetadata: {
            select: {
              noOfCanceledDeliveries: true,
              noOfSuccessfullDeliveries: true,
              averageRating: true,
            },
          },
        },
        // where: {
        // AND: [?
        // { vendorId: requestBody.headers.vendorId },
        // {
        //   assignedUserBranch: {
        //     every: {
        //       AND: { branchId: filter.branchId },
        //     },
        //   },
        // },
        // { roleId: deRole },
        // { status: 'ACTIVE' },
        // ],
        // },
      });
      // const deliveryExecutives = await db.user.findMany(query);

      deliveryExecutives.forEach((exec: any, index) => {
        exec.position = LAT_LONG[index];
      });

      const deliveryExecutivesCount = db.user.count({
        select: {
          id: true,
          name: true,
          phoneNumber: true,
        },
        where: {
          AND: [
            { vendorId: requestBody.headers.vendorId },
            { roleId: deRole },
            { status: 'ACTIVE' },
          ],
        },
      });

      res.status(CommonConstants.SUCCESS_CODE.OK).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.getUsers,
        data: {
          records: deliveryExecutives,
          totalCount: deliveryExecutivesCount,
        },
      });
    } catch (error) {
      console.error(error);
      logger.error(`Got error while inserting new vendor -> ${error.message}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }
}
