import { Request, Response, NextFunction } from 'express';
import LoggerFactory from '../helpers/LoggerFactory';
import { ObjectUtil } from '../helpers/ObjectUtil';
import { CommonConstants } from '../helpers/CommonConstants';
import { StatusMessages } from '../helpers/StatusMessages';
import { PrismaConnectionService } from '../service/PrismaConnectionService';
import { Prisma } from '@prisma/client';
import { getPagination } from '../helpers/helperFunctions';
import { v4 as uuidv4 } from 'uuid';
import { OrderStatusController } from './OrderStatusController';

let logger = LoggerFactory.getLogger(module);

export class OrderController {
  async createOrder(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info(`About to insert new order ${JSON.stringify(req.body)}`);
      const requestBody = req.body;

      if (
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.orderNumber) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.discountPercent) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.branch) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.paymentMode) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.deliverySlotFrom) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.deliverySlotTo) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.customerPhoneNumber) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.customerAddressId) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.gstPercent) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.deliveryDate) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.orderItems)
      ) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      const db = PrismaConnectionService.getDBConnection();

      const checkIfOrderExist = await db.order.findFirst({
        where: {
          AND: [
            {
              vendorId: requestBody.headers.vendorId,
            },
            {
              orderNumber: requestBody.orderNumber,
            },
          ],
        },
      });

      if (ObjectUtil.isNotNullAndUndefined(checkIfOrderExist)) {
        let validationError = {};
        validationError['orderNumber'] = StatusMessages.orderExist;
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.wentWrong,
          data: validationError,
        });
        return next();
      }

      let netAmount = 0;
      let totalAmount = 0;
      let orderItems: Array<Prisma.orderItemCreateManyOrderInput> = [];

      requestBody.orderItems.forEach((order) => {
        totalAmount += order.quantity * order.unit;
      });

      let discountAmount = (totalAmount / 100) * requestBody.discountPercent;
      let gstAmount = (totalAmount / 100) * requestBody.gstPercent;
      netAmount = totalAmount - discountAmount + gstAmount;

      requestBody.orderItems.forEach((order) => {
        orderItems.push({
          name: order.name,
          quantity: order.quantity,
          unit: parseFloat(order.unit),
          vendorId: requestBody.headers.vendorId as string,
          branchId: requestBody.branch,
        });
      });

      const query1: Prisma.customerFindFirstArgs = {
        where: {
          phoneNumber: requestBody.customerPhoneNumber,
        },
        select: {
          id: true,
          phoneNumber: true,
          name: true,
        },
      };
      const customerDetails = await db.customer.findFirst(query1);

      const customerAddressDetails = await db.customerAddress.findFirst({
        where: {
          id: requestBody.customerAddressId,
          // vendorId: requestBody.headers.vendorId as string,
        },
        select: {
          id: true,
          name: true,
          address: true,
          landmark: true,
          pincode: true,
          state: true,
          city: true,
          country: true,
          phoneNumber: true,
          lat: true,
          long: true,
          status: true,
          createdAt: true,
        },
      });

      let createOrder: Prisma.orderCreateInput = {
        referenceOrderNumber: requestBody.orderNumber,
        orderNumber: uuidv4().substring(0, 5),
        createdBy: requestBody.headers.vendorId,
        status: requestBody.status,
        discountPercent: requestBody.discountPercent,
        discountAmount,
        totalAmount,
        gstPercent: requestBody.gstPercent,
        netAmount,
        paymentMode: requestBody.paymentMode,
        deliverySlotFrom: requestBody.deliverySlotFrom,
        deliverySlotTo: requestBody.deliverySlotTo,
        orderType: 'DELIMILE',
        deliveryDate: new Date(requestBody.deliveryDate),
        orderDeliveryAddress: {
          create: {
            name: customerDetails.name,
            address: customerAddressDetails.address,
            landmark: customerAddressDetails.landmark,
            pincode: customerAddressDetails.pincode,
            state: customerAddressDetails.state,
            city: customerAddressDetails.city,
            country: customerAddressDetails.country,
            phoneNumber: customerAddressDetails.phoneNumber,
            lat: customerAddressDetails.lat,
            long: customerAddressDetails.long,
            vendor: {
              connect: {
                id: requestBody.headers.vendorId as string,
              },
            },
            branch: {
              connect: {
                id: requestBody.branch,
              },
            },
            customer: {
              connect: {
                id: customerDetails.id,
              },
            },
          },
        },
        orderDeliveryDetails: {
          create: {},
        },
        vendor: {
          connect: {
            id: requestBody.headers.vendorId as string,
          },
        },
        branch: {
          connect: {
            id: requestBody.branch,
          },
        },
        orderItem: {
          createMany: {
            data: orderItems,
          },
        },
      };

      const orderDetails = await db.order.create({
        data: createOrder,
        select: {
          id: true,
        },
      });

      const updatedOrderDetails = await db.order.findFirstOrThrow({
        where: {
          id: orderDetails.id,
        },
        select: {
          id: true,
          vendorId: true,
          branchId: true,
          orderNumber: true,
          discountPercent: true,
          totalAmount: true,
          gstPercent: true,
          netAmount: true,
          paymentMode: true,
          deliverySlotTo: true,
          deliverySlotFrom: true,
          discountAmount: true,
          deliveryDate: true,
          createdAt: true,
          referenceOrderNumber: true,
          status: true,
          orderItem: true,
          orderDeliveryAddress: true,
        },
      });

      const createdUser = await db.user.findUnique({
        where: {
          id: requestBody.headers.userId,
        },
        select: {
          name: true,
          id: true,
        },
      });

      await new OrderStatusController().updateOrderHistory({
        branchId: requestBody.headers.branchId,
        vendorId: requestBody.headers.vendorId,
        orderId: orderDetails.id,
        remarks: `Created order from Delime dashboard by <strong>${createdUser.name}(${createdUser.id})</strong>`,
        status: 'ASSIGNED_DE',
        createdBy: createdUser.name,
      });

      res.status(CommonConstants.SUCCESS_CODE.CREATED).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.newOrderCreated,
        data: updatedOrderDetails,
      });
    } catch (error) {
      logger.error(`Error trace back -> ${error}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }

  async assignDE(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info(`About to assign DE ${JSON.stringify(req.body)}`);
      const requestBody = req.body;

      if (
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.orderId) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.deUserId)
      ) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      const db = PrismaConnectionService.getDBConnection();

      const orderDetails = await db.order.findFirst({
        where: {
          id: requestBody.orderId,
          vendorId: requestBody.headers.vendorId,
        },
        select: {
          id: true,
          deliverySlotFrom: true,
          deliverySlotTo: true,
          deliveryDate: true,
        },
      });

      if (ObjectUtil.isNullOrUndefinedOrEmpty(orderDetails)) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.wentWrong,
          data: {},
        });
        return next();
      }

      const query: Prisma.orderUpdateInput = {
        status: 'ASSIGNED_DE',
        assignedOrderDetailsToDE: {
          create: {
            fromTime: orderDetails.deliverySlotFrom,
            deliveryDate: orderDetails.deliveryDate,
            toTime: orderDetails.deliverySlotTo,
            userId: requestBody.deUserId,
          },
        },
        orderDeliveryDetails: {
          update: {
            where: {
              orderId: orderDetails.id,
            },
            data: {
              deliveryExecutive: {
                connect: {
                  id: requestBody.deUserId,
                },
              },
            },
          },
        },
      };

      await db.order.update({
        where: {
          id: orderDetails.id,
        },
        data: query,
      });

      const user = await db.user.findUnique({
        where: {
          id: requestBody.deUserId,
        },
        select: {
          name: true,
          id: true,
        },
      });

      await new OrderStatusController().updateOrderHistory({
        branchId: requestBody.headers.branchId,
        vendorId: requestBody.headers.vendorId,
        orderId: orderDetails.id,
        remarks: `<strong>${requestBody.headers.userName}(${requestBody.headers.userId})</strong> assigned <strong>${user.name}(${user.id})</strong> as DE`,
        status: 'ASSIGNED_DE',
        createdBy: requestBody.headers.userName,
      });

      res.status(CommonConstants.SUCCESS_CODE.CREATED).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.assignedDE,
        data: {},
      });
    } catch (error) {
      logger.error(`Error trace back -> ${error}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }

  async updateDE(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info(`About to update DE ${JSON.stringify(req.body)}`);
      const requestBody = req.body;

      if (
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.orderId) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.deUserId)
      ) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      const db = PrismaConnectionService.getDBConnection();

      const orderDetails = await db.order.findFirst({
        where: {
          id: +requestBody.orderId,
          vendorId: requestBody.headers.vendorId,
        },
        select: {
          id: true,
          deliverySlotFrom: true,
          deliverySlotTo: true,
          deliveryDate: true,
          assignedOrderDetailsToDE: {
            where: {
              orderId: +requestBody.orderId,
            },
            select: {
              id: true,
            },
          },
        },
      });

      if (ObjectUtil.isNullOrUndefinedOrEmpty(orderDetails)) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.wentWrong,
          data: {},
        });
        return next();
      }

      const query: Prisma.orderUpdateInput = {
        status: 'ASSIGNED_DE',
        assignedOrderDetailsToDE: {
          update: {
            where: {
              id: orderDetails.assignedOrderDetailsToDE[0].id,
            },
            data: {
              userId: requestBody.deUserId,
            },
          },
        },
        orderDeliveryDetails: {
          update: {
            where: {
              orderId: orderDetails.id,
            },
            data: {
              deliveryExecutive: {
                connect: {
                  id: requestBody.deUserId,
                },
              },
            },
          },
        },
      };

      await db.order.update({
        where: {
          id: orderDetails.id,
        },
        data: query,
      });

      const user = await db.user.findUnique({
        where: {
          id: requestBody.deUserId,
        },
        select: {
          name: true,
          id: true,
        },
      });

      await new OrderStatusController().updateOrderHistory({
        branchId: requestBody.headers.branchId,
        vendorId: requestBody.headers.vendorId,
        orderId: orderDetails.id,
        remarks: `<strong>${requestBody.headers.userName}(${requestBody.headers.userId})</strong> updated <strong>${user.name}(${user.id})</strong> as DE`,
        status: 'CREATED',
        createdBy: requestBody.headers.userName,
      });

      res.status(CommonConstants.SUCCESS_CODE.CREATED).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.updatedDE,
        data: {},
      });
    } catch (error) {
      logger.error(`Error trace back -> ${error}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }

  async getOrderByVendorId(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info(`About to get order details ${JSON.stringify(req.body)}`);
      const requestBody = req.body;
      let sortOrder = { id: 'asc' };
      const { pageNo, pageSize, search, filter } = requestBody;
      const { limit, offset } = getPagination(+pageNo, +pageSize);
      let status = 'CREATED';

      if (requestBody.sortOrder) {
        sortOrder = requestBody.sortOrder;
      }

      if (filter?.status) {
        status = filter.status;
      }

      const searchableFields = [];
      const searchQuery = [];

      if (search) {
        searchableFields.forEach((field) => {
          searchQuery.push({
            [field]: { contains: search, mode: 'insensitive' },
          });
        });
      }

      let query: any = null;

      let branchId =
        requestBody.headers.accessType === 'ALL'
          ? filter.branch === null
            ? 'ALL'
            : filter.branch
          : requestBody.headers.branchId;

      if (branchId === 'ALL') {
        query = {
          skip: offset,
          take: limit,
          orderBy: sortOrder as any,
          where: {
            AND: [{ vendorId: requestBody.headers.vendorId }, { status }],
          },
        };
      } else {
        query = {
          skip: offset,
          take: limit,
          orderBy: sortOrder as any,
          where: {
            AND: [
              { vendorId: requestBody.headers.vendorId },
              { branchId: branchId },
              { status },
            ],
          },
        };
      }

      if (searchQuery.length > 0) {
        if (branchId === 'ALL') {
          query.where = {
            AND: [{ vendorId: requestBody.headers.vendorId }, { status }],
            OR: [...searchQuery],
          };
        } else {
          query.where = {
            AND: [
              { vendorId: requestBody.headers.vendorId },
              { branchId: branchId },
              { status },
            ],
            OR: [...searchQuery],
          };
        }
      }

      query.select = {
        id: true,
        vendorId: true,
        branchId: true,
        orderNumber: true,
        discountPercent: true,
        totalAmount: true,
        gstPercent: true,
        netAmount: true,
        paymentMode: true,
        deliverySlotTo: true,
        deliverySlotFrom: true,
        discountAmount: true,
        deliveryDate: true,
        createdAt: true,
        referenceOrderNumber: true,
        status: true,
        orderItem: true,
        orderDeliveryDetails: {
          select: {
            deliveryExecutive: {
              select: {
                name: true,
                id: true,
                phoneNumber: true,
              },
            },
            deliveryFeedback: true,
            deliveryPickUpTime: true,
            deliveryFeedbackRating: true,
            deliveredTime: true,
          },
        },
        orderDeliveryAddress: {
          select: {
            id: true,
            customer: {
              select: {
                name: true,
                phoneNumber: true,
                id: true,
              },
            },
            name: true,
            address: true,
            landmark: true,
            pincode: true,
            state: true,
            city: true,
            country: true,
            phoneNumber: true,
            lat: true,
            long: true,
          },
        },
        branch: {
          select: {
            branchName: true,
            address1: true,
            address2: true,
            city: true,
            country: true,
            pincode: true,
            state: true,
            landmark: true,
            phoneNumber1: true,
            phoneNumber2: true,
            email: true,
          },
        },
      };
      const db = PrismaConnectionService.getDBConnection();

      const orderDetails = await db.order.findMany(query);

      let countQuery = query;
      delete countQuery.skip;
      delete countQuery.select;
      delete countQuery.take;

      const ordersCount = await db.order.count(query);

      res.status(CommonConstants.SUCCESS_CODE.OK).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.getOrders,
        data: { records: orderDetails, totalCount: ordersCount },
      });
    } catch (error) {
      logger.error(`Error trace back -> ${error}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }

  async getOrderByOrderId(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info(`About to get order details ${JSON.stringify(req.body)}`);
      const requestBody = req.body;

      if (ObjectUtil.isNullOrUndefinedOrEmpty(req.params.id)) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      let query: Prisma.orderFindFirstArgs = {
        where: {
          AND: [
            { vendorId: requestBody.headers.vendorId },
            { id: +req.params.id },
          ],
        },
        select: {
          id: true,
          vendorId: true,
          branchId: true,
          orderNumber: true,
          discountPercent: true,
          totalAmount: true,
          gstPercent: true,
          netAmount: true,
          paymentMode: true,
          deliverySlotTo: true,
          deliverySlotFrom: true,
          discountAmount: true,
          deliveryDate: true,
          createdAt: true,
          referenceOrderNumber: true,
          status: true,
          orderItem: true,
          orderDeliveryDetails: {
            select: {
              deliveryExecutive: {
                select: {
                  name: true,
                  id: true,
                  phoneNumber: true,
                  email: true,
                  status: true,
                },
              },
              deliveryFeedback: true,
              deliveryPickUpTime: true,
              deliveryFeedbackRating: true,
              cancelledOrReturnReason: true,
              deliveredTime: true,
            },
          },
          track_order: {
            where: {
              AND: [
                { vendorId: requestBody.headers.vendorId },
                { orderId: +req.params.id },
                { status: 'ASSIGNED_DE' },
              ],
            },
            orderBy: {
              id: 'desc',
            },
            select: {
              status: true,
              createdAt: true,
              createdBy: true,
            },
            take: 1,
            skip: 0,
          },
          orderDeliveryAddress: {
            select: {
              id: true,
              customer: {
                select: {
                  name: true,
                  phoneNumber: true,
                  id: true,
                },
              },
              name: true,
              address: true,
              landmark: true,
              pincode: true,
              state: true,
              city: true,
              country: true,
              phoneNumber: true,
              lat: true,
              long: true,
            },
          },
          branch: {
            select: {
              branchName: true,
              address1: true,
              address2: true,
              city: true,
              country: true,
              pincode: true,
              state: true,
              landmark: true,
              phoneNumber1: true,
              phoneNumber2: true,
              email: true,
            },
          },
        },
      };

      const db = PrismaConnectionService.getDBConnection();
      const orderDetails = await db.order.findFirst(query);

      res.status(CommonConstants.SUCCESS_CODE.OK).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.getOrders,
        data: { record: orderDetails },
      });
    } catch (error) {
      logger.error(`Error trace back -> ${error}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }
}
