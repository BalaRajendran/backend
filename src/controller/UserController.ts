import { Request, Response, NextFunction } from 'express';
import { CommonConstants } from '../helpers/CommonConstants';
import { Prisma } from '@prisma/client';

import LoggerFactory from '../helpers/LoggerFactory';
import { ObjectUtil } from '../helpers/ObjectUtil';
import { StatusMessages } from '../helpers/StatusMessages';
import { PrismaConnectionService } from '../service/PrismaConnectionService';
import { getPagination } from '../helpers/helperFunctions';

let logger = LoggerFactory.getLogger(module);

export class UserController {
  async createUser(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info(`About to insert new user ${JSON.stringify(req.body)}`);
      const requestBody = req.body;
      if (
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.userName) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.roleId) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.email) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.phoneNumber) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.password) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.accessType) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.status)
      ) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      const db = PrismaConnectionService.getDBConnection();
      const checkIfUserExist = await db.user.findFirst({
        where: {
          OR: [
            {
              email: requestBody.email,
              phoneNumber: requestBody.phoneNumber,
            },
          ],
          AND: [
            {
              vendorId: requestBody.vendorId,
            },
          ],
        },
      });

      if (ObjectUtil.isNotNullAndUndefined(checkIfUserExist)) {
        let validationError = {};
        if (checkIfUserExist.phoneNumber === requestBody.phoneNumber) {
          validationError['phoneNumber'] = StatusMessages.phoneNumberExist;
        }

        if (checkIfUserExist.email === requestBody.email) {
          validationError['email'] = StatusMessages.emailExist;
        }
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.wentWrong,
          data: validationError,
        });
        return next();
      }

      // validation check with phone number
      const checkIfBusinessExist = await db.vendor.findFirst({
        where: {
          OR: [
            {
              email: requestBody.email,
              phoneNumber: requestBody.phoneNumber,
            },
          ],
          AND: [
            {
              id: requestBody.vendorId,
            },
          ],
        },
      });

      if (ObjectUtil.isNotNullAndUndefined(checkIfBusinessExist)) {
        let validationError = {};
        if (checkIfBusinessExist.phoneNumber === requestBody.phoneNumber) {
          validationError['phoneNumber'] = StatusMessages.phoneNumberExist;
        }

        if (checkIfBusinessExist.email === requestBody.email) {
          validationError['email'] = StatusMessages.emailExist;
        }
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.wentWrong,
          data: validationError,
        });
        return next();
      }

      if (!new UserController().createUserFromRequest(requestBody)) {
        res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.wentWrong,
        });
      }

      res.status(CommonConstants.SUCCESS_CODE.CREATED).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.newUserCreated,
      });
    } catch (error) {
      console.error(error);
      logger.error(`Got error while inserting new vendor -> ${error.message}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }

  async createUserFromRequest(requestBody) {
    try {
      let user: Prisma.userCreateInput = {
        password: requestBody.password,
        email: requestBody.email,
        accessType: requestBody.accessType,
        phoneNumber: requestBody.phoneNumber,
        name: requestBody.userName,
        lastLoginTime: new Date(),
        createdBy: requestBody.headers.vendorId,
        status: requestBody.status,
        vendor: {
          connect: {
            id: requestBody.headers.vendorId as string,
          },
        },
        role: {
          connect: {
            id: requestBody.roleId,
          },
        },
      };

      const db = PrismaConnectionService.getDBConnection();

      const userDetails = await db.user.create({
        data: user,
        select: {
          id: true,
        },
      });

      if (requestBody.roleId === 3) {
        await db.deliveryUserMetadata.create({
          data: {
            user: {
              connect: {
                id: userDetails.id,
              },
            },
          },
        });
      }

      if (
        userDetails &&
        requestBody.accessType === CommonConstants.BRANCH_TYPE.INDIVIDUAL
      ) {
        let userBranch = [];
        requestBody.assignedBranch.forEach((branchId: string) => {
          userBranch.push({
            userId: userDetails.id,
            branchId,
          });
        });

        await db.assignedUserBranch.createMany({
          data: userBranch,
        });

        return true;
      }
      return false;
    } catch (error) {
      return false;
    }
  }

  async getUsers(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info(`About to get users`);
      let sortOrder = { id: 'asc' };

      const requestBody = req.body;
      const { pageNo, pageSize, search } = requestBody;
      const { limit, offset } = getPagination(+pageNo, +pageSize);
      if (requestBody.sortOrder) {
        sortOrder = requestBody.sortOrder;
      }
      const db = PrismaConnectionService.getDBConnection();

      const searchableFields = ['email', 'phoneNumber', 'name'];
      const searchQuery = [];

      if (search) {
        searchableFields.forEach((field) => {
          searchQuery.push({
            [field]: { contains: search, mode: 'insensitive' },
          });
        });
      }

      let whereCondition: any = null;

      if (requestBody.headers.branchId === CommonConstants.BRANCH_TYPE.ALL) {
        whereCondition = {
          AND: [{ vendorId: requestBody.headers.vendorId }],
        };

        if (searchQuery.length > 0) {
          whereCondition = {
            AND: [{ vendorId: requestBody.headers.vendorId }],
            OR: [...searchQuery],
          };
        }
      } else {
        whereCondition = {
          AND: [
            { vendorId: requestBody.headers.vendorId },
            // { accessType: CommonConstants.BRANCH_TYPE.INDIVIDUAL as any },
            {
              assignedUserBranch: {
                every: {
                  branchId: requestBody.headers.branchId,
                },
              },
            },
          ],
        };

        if (searchQuery.length > 0) {
          whereCondition = {
            AND: [
              { vendorId: requestBody.headers.vendorId },
              // { accessType: CommonConstants.BRANCH_TYPE.INDIVIDUAL },
              {
                assignedUserBranch: {
                  every: {
                    branchId: requestBody.headers.branchId,
                  },
                },
              },
            ],
            OR: [...searchQuery],
          };
        }
      }

      let query: any = {
        skip: offset,
        take: limit,
        orderBy: sortOrder as any,
        where: whereCondition,
      };

      query.select = {
        id: true,
        name: true,
        email: true,
        phoneNumber: true,
        assignedUserBranch: {
          select: {
            branchId: true,
          },
        },
        role: {
          select: {
            id: true,
            name: true,
          },
        },
        accessType: true,
        status: true,
      };

      const users = await db.user.findMany(query);

      let countQuery = query;
      delete countQuery.skip;
      delete countQuery.select;
      delete countQuery.take;

      const usersCount = await db.user.count(query);

      res.status(CommonConstants.SUCCESS_CODE.OK).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.getUsers,
        data: { records: users, totalCount: usersCount },
      });
    } catch (error) {
      console.error(error);
      logger.error(`Got error while inserting new vendor -> ${error.message}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }

  async availableDeliveryExecutives(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      logger.info(`About to get available DE`);
      let sortOrder = { id: 'asc' };
      const requestBody = req.body;
      const { pageNo, pageSize, search, filter } = requestBody;
      const { limit, offset } = getPagination(+pageNo, +pageSize);

      if (
        ObjectUtil.isNullOrUndefinedOrEmpty(filter.deliveryDate) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(filter.from) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(filter.branchId) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(filter.to)
      ) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      const response = await new UserController().getDEfromDB(requestBody);

      res.status(CommonConstants.SUCCESS_CODE.OK).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.getUsers,
        data: response,
      });
    } catch (error) {
      console.error(error);
      logger.error(`Got error while inserting new vendor -> ${error.message}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }

  getDEfromDB = async (requestBody: any) => {
    let sortOrder = { id: 'asc' };
    const { pageNo, pageSize, search, filter } = requestBody;
    const { limit, offset } = getPagination(+pageNo, +pageSize);

    if (requestBody.sortOrder) {
      sortOrder = requestBody.sortOrder;
    }

    const db = PrismaConnectionService.getDBConnection();
    const deRole = 3;
    const searchableFields = ['email', 'phoneNumber', 'name'];
    const searchQuery = [];

    if (search) {
      searchableFields.forEach((field) => {
        searchQuery.push({
          [field]: { contains: search, mode: 'insensitive' },
        });
      });
    }

    let whereCondition: any = null;

    if (requestBody.headers.branchId === CommonConstants.BRANCH_TYPE.ALL) {
      whereCondition = {
        AND: [
          { vendorId: requestBody.headers.vendorId },
          { roleId: deRole },
          { status: 'ACTIVE' },
          {
            assignedOrderDetailsToDE: {
              every: {
                NOT: {
                  fromTime: requestBody.filter.from,
                  toTime: requestBody.filter.to,
                  deliveryDate: new Date(requestBody.filter.deliveryDate),
                },
              },
            },
          },
        ],
      };

      if (searchQuery.length > 0) {
        whereCondition = {
          AND: [
            { vendorId: requestBody.headers.vendorId },
            { roleId: deRole },
            { status: 'ACTIVE' },
            {
              assignedOrderDetailsToDE: {
                every: {
                  NOT: {
                    fromTime: requestBody.filter.from,
                    toTime: requestBody.filter.to,
                    deliveryDate: new Date(requestBody.filter.deliveryDate),
                  },
                },
              },
            },
          ],
          OR: [...searchQuery],
        };
      }
    } else {
      whereCondition = {
        AND: [
          { vendorId: requestBody.headers.vendorId },
          { roleId: deRole },
          { status: 'ACTIVE' },
          {
            assignedOrderDetailsToDE: {
              every: {
                NOT: {
                  fromTime: filter.from,
                  toTime: filter.to,
                  deliveryDate: new Date(filter.deliveryDate),
                },
              },
            },
          },
          // { accessType: CommonConstants.BRANCH_TYPE.INDIVIDUAL as any },
          {
            assignedUserBranch: {
              every: {
                branchId: filter.branchId,
              },
            },
          },
        ],
      };

      if (searchQuery.length > 0) {
        whereCondition = {
          AND: [
            { vendorId: requestBody.headers.vendorId },
            { roleId: deRole },
            { status: 'ACTIVE' },
            {
              assignedOrderDetailsToDE: {
                every: {
                  NOT: {
                    fromTime: filter.from,
                    toTime: filter.to,
                    deliveryDate: new Date(filter.deliveryDate),
                  },
                },
              },
            },
            {
              assignedUserBranch: {
                every: {
                  branchId: filter.branchId,
                },
              },
            },
          ],
          OR: [...searchQuery],
        };
      }
    }

    let query: any = {
      skip: offset,
      take: limit,
      orderBy: sortOrder as any,
      where: whereCondition,
    };

    query.select = {
      id: true,
      name: true,
      phoneNumber: true,
    };

    const users = await db.user.findMany(query);

    let countQuery = query;
    delete countQuery.skip;
    delete countQuery.select;
    delete countQuery.take;

    const usersCount = await db.user.count(query);

    return { records: users, totalCount: usersCount };
  };
}
