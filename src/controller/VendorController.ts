import { Request, Response, NextFunction } from 'express';
import { CommonConstants } from '../helpers/CommonConstants';
import { Prisma } from '@prisma/client';

import LoggerFactory from '../helpers/LoggerFactory';
import { ObjectUtil } from '../helpers/ObjectUtil';
import { StatusMessages } from '../helpers/StatusMessages';
import { PrismaConnectionService } from '../service/PrismaConnectionService';
import { stringSlugify } from '../helpers/helperFunctions';
import { uuid } from 'uuidv4';
import { LoginController } from './LoginController';

let logger = LoggerFactory.getLogger(module);

const selectFields = {
  id: true,
  businessName: true,
  businessType: true,
  email: true,
  phoneNumber: true,
  avatar: true,
  gstNo: true,
  lat: true,
  long: true,
  status: true,
  branch: {
    select: {
      id: true,
      branchName: true,
      email: true,
      address1: true,
      address2: true,
      city: true,
      state: true,
      country: true,
      pincode: true,
      phoneNumber1: true,
      phoneNumber2: true,
      status: true,
      startsFrom: true,
      closeOn: true,
      pickupStartsFrom: true,
      pickupCloseAt: true,
      weekOffDays: true,
      lat: true,
      long: true,
    },
  },
};

export class VendorController {
  async getVendorById(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info(`About to get vendor ${req.params['id']}`);
      const id: any = req.params.id;

      if (ObjectUtil.isNullOrUndefinedOrEmpty(id)) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      const db = PrismaConnectionService.getDBConnection();

      const vendor = await db.vendor.findUnique({
        where: {
          id,
        },
        select: {
          id: true,
          businessName: true,
          businessType: true,
          email: true,
          phoneNumber: true,
          avatar: true,
          gstNo: true,
          lat: true,
          long: true,
          status: true,
          branch: {
            select: {
              id: true,
              branchName: true,
              email: true,
              address1: true,
              address2: true,
              city: true,
              state: true,
              country: true,
              pincode: true,
              phoneNumber1: true,
              phoneNumber2: true,
              status: true,
              startsFrom: true,
              closeOn: true,
              pickupStartsFrom: true,
              pickupCloseAt: true,
              weekOffDays: true,
              lat: true,
              long: true,
            },
          },
        },
      });

      if (ObjectUtil.isNullOrUndefinedOrEmpty(vendor)) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.badRequest,
        });
        return next();
      }

      res.status(CommonConstants.SUCCESS_CODE.OK).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.getVendor,
        data: vendor,
      });
    } catch (error) {
      console.error(error);
      logger.error(`Got error while inserting new vendor -> ${error.message}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }

  async createVendor(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info(`About to insert new vendor ${JSON.stringify(req.body)}`);
      const requestBody = req.body;

      if (
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.businessName) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.email) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.phoneNumber) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.businessType) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.password)
      ) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      const db = PrismaConnectionService.getDBConnection();
      const checkIfBusinessExist = await db.vendor.findFirst({
        where: {
          OR: [
            { email: requestBody.email },
            { phoneNumber: requestBody.phoneNumber },
          ],
        },
      });

      if (ObjectUtil.isNotNullAndUndefined(checkIfBusinessExist)) {
        let validationError = {};
        if (checkIfBusinessExist.phoneNumber === requestBody.phoneNumber) {
          validationError['phoneNumber'] = StatusMessages.phoneNumberExist;
        }

        if (checkIfBusinessExist.email === requestBody.email) {
          validationError['email'] = StatusMessages.emailExist;
        }
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.wentWrong,
          data: validationError,
        });
        return next();
      }

      let slug = stringSlugify(requestBody.businessName);
      const checkForSlug = await db.vendor.findFirst({
        where: {
          id: slug,
        },
      });

      if (ObjectUtil.isNotNullAndUndefined(checkForSlug)) {
        slug += '-' + uuid().slice(0, 4);
      }

      let newVendor: Prisma.vendorCreateInput = {
        id: slug,
        businessName: requestBody.businessName,
        email: requestBody.email,
        phoneNumber: requestBody.phoneNumber,
        businessType: requestBody.businessType,
      };

      const insertResponse = await db.vendor.create({
        data: newVendor,
        select: selectFields,
      });

      let newUser: Prisma.userCreateInput = {
        email: requestBody.email,
        phoneNumber: requestBody.phoneNumber,
        password: requestBody.password,
        name: slug,
        accessType: 'ALL',
        lastLoginTime: new Date(),
        status: 'ACTIVE',
        vendor: {
          connect: {
            id: insertResponse.id,
          },
        },
        role: {
          connect: {
            id: 4,
          },
        },
      };

      await db.user.create({
        data: newUser,
        select: {
          name: true,
          id: true,
          accessType: true,
          email: true,
        },
      });

      const response = await new LoginController().loginUser({
        businessName: slug,
        password: requestBody.password,
        phoneNumber: requestBody.phoneNumber,
      });

      res.status(CommonConstants.SUCCESS_CODE.CREATED).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.newVendorCreated,
        data: response.data,
      });
    } catch (error) {
      console.error(error);
      logger.error(`Got error while inserting new vendor -> ${error.message}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }

  async updateVendor(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info(`About to update vendor ${JSON.stringify(req.body)}`);
      const requestBody = req.body;

      if (
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.businessName) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.businessType) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.phoneNumber) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.email)
      ) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      const db = PrismaConnectionService.getDBConnection();
      let user: Prisma.vendorUpdateInput = {
        businessName: requestBody.businessName,
        avatar: requestBody.avatar,
        businessType: requestBody.businessType,
        gstNo: requestBody.gstNo,
      };
      await db.vendor.update({
        where: {
          phoneNumber: requestBody.phoneNumber,
        },
        data: user,
      });

      res.status(CommonConstants.SUCCESS_CODE.CREATED).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.updateVendorCreated,
      });
    } catch (error) {
      console.error(error);
      logger.error(`Got error while inserting new vendor -> ${error.message}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }

  async deleteVendorById(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info(`About to delete vendor ${req.params['id']}`);
      const id: any = req.params.id;

      if (ObjectUtil.isNullOrUndefinedOrEmpty(id)) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      const db = PrismaConnectionService.getDBConnection();
      await db.vendor.delete({
        where: {
          id,
        },
      });

      res.status(CommonConstants.SUCCESS_CODE.CREATED).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.deleteVendor,
      });
    } catch (error) {
      console.error(error);
      logger.error(`Got error while inserting new vendor -> ${error.message}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }
}
