import { Request, Response, NextFunction } from 'express';
import { CommonConstants } from '../helpers/CommonConstants';
import LoggerFactory from '../helpers/LoggerFactory';
import { ObjectUtil } from '../helpers/ObjectUtil';
import { StatusMessages } from '../helpers/StatusMessages';
import { PrismaConnectionService } from '../service/PrismaConnectionService';

let logger = LoggerFactory.getLogger(module);

const selectFields = {
  id: true,
  businessName: true,
  businessType: true,
  email: true,
  phoneNumber: true,
  avatar: true,
  gstNo: true,
  lat: true,
  long: true,
  status: true,
  branch: {
    select: {
      id: true,
      branchName: true,
      email: true,
      address1: true,
      address2: true,
      city: true,
      state: true,
      country: true,
      pincode: true,
      phoneNumber1: true,
      phoneNumber2: true,
      status: true,
      startsFrom: true,
      closeOn: true,
      pickupStartsFrom: true,
      pickupCloseAt: true,
      weekOffDays: true,
      lat: true,
      long: true,
    },
  },
};

export class LoginController {
  async vendorLogin(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info(`About to login ${JSON.stringify(req.body)}`);
      const requestBody = req.body;

      if (
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.businessName) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.phoneNumber) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.password)
      ) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      const loginResponse = await new LoginController().loginUser(requestBody);

      res.status(loginResponse.statusCode).send({
        status: loginResponse.status,
        message: loginResponse.message,
        data: loginResponse.data,
      });
    } catch (error) {
      console.error(error);
      logger.error(`Got error while inserting new vendor -> ${error.message}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }

  async loginUser(requestBody: {
    businessName: string;
    phoneNumber: string;
    password: string;
  }) {
    const db = PrismaConnectionService.getDBConnection();

    let loginUserDetails: any = await db.user.findFirst({
      where: {
        vendorId: requestBody.businessName,
        phoneNumber: requestBody.phoneNumber,
      },
      select: {
        id: true,
        name: true,
        role: true,
        accessType: true,
        email: true,
        password: true,
      },
    });

    if (ObjectUtil.isNullOrUndefinedOrEmpty(loginUserDetails)) {
      return {
        status: CommonConstants.ERROR,
        statusCode: CommonConstants.ERROR_CODE.BAD_REQUEST,
        message: StatusMessages.accountNotFound,
        data: { phoneNumber: StatusMessages.accountNotFound },
      };
    }

    if (loginUserDetails.password !== requestBody.password) {
      return {
        status: CommonConstants.ERROR,
        statusCode: CommonConstants.ERROR_CODE.BAD_REQUEST,
        message: StatusMessages.invalidPassword,
        data: { password: StatusMessages.invalidPassword },
      };
    }

    let vendorDetails = null;
    let userDetails = null;

    userDetails = loginUserDetails;

    const branch = await db.assignedUserBranch.findMany({
      where: {
        userId: userDetails.id as number,
      },
      select: {
        branchId: true,
      },
    });

    userDetails.branch = branch.map((branch) => branch.branchId);

    vendorDetails = await db.vendor.findFirst({
      where: {
        id: requestBody.businessName,
      },
      select: selectFields,
    });
    return {
      status: CommonConstants.SUCCESS,
      statusCode: CommonConstants.SUCCESS_CODE.OK,
      message: StatusMessages.loginVendor,
      data: {
        vendorDetails,
        userDetails,
      },
    };
  }
}
