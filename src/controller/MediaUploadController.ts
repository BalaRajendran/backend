import { NextFunction, Request, Response } from 'express';
import { v4 as uuidv4 } from 'uuid';
import { CommonConstants } from '../helpers/CommonConstants';
import LoggerFactory from '../helpers/LoggerFactory';
import { ObjectUtil } from '../helpers/ObjectUtil';
import { StatusMessages } from '../helpers/StatusMessages';

const express = require('express');
const router = express.Router();
const multer = require('multer');
const logger = LoggerFactory.getLogger(module);

const DIR = './public/';
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, DIR);
  },
  filename: (req, file, cb) => {
    const fileName = file.originalname.toLowerCase().split(' ').join('-');
    cb(null, uuidv4() + '-' + fileName);
  },
});
const upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == 'image/png' ||
      file.mimetype == 'image/jpg' ||
      file.mimetype == 'image/jpeg'
    ) {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
    }
  },
});

router.post(
  '/upload/:id',
  upload.single('file'),
  async (req: any, res: Response, next: NextFunction) => {
    try {
      logger.info(
        'Got request inside uploadMedia ' + JSON.stringify(req.params)
      );

      if (ObjectUtil.isNullOrUndefinedOrEmpty(req.params.id)) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      const url =
        req.protocol + '://' + req.get('host') + '/' + req.file.filename;

      res.status(CommonConstants.SUCCESS_CODE.CREATED).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.imageUploaded,
        data: {
          url,
        },
      });
    } catch (error) {
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
      return next();
    }
  }
);
module.exports = router;
