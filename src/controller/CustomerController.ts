import { Request, Response, NextFunction } from 'express';
import { CommonConstants } from '../helpers/CommonConstants';
import { Prisma } from '@prisma/client';

import LoggerFactory from '../helpers/LoggerFactory';
import { ObjectUtil } from '../helpers/ObjectUtil';
import { StatusMessages } from '../helpers/StatusMessages';
import { PrismaConnectionService } from '../service/PrismaConnectionService';
import { getPagination } from '../helpers/helperFunctions';

let logger = LoggerFactory.getLogger(module);

export class CustomerController {
  async getCustomerAddressByPhoneNumber(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      logger.info(`About to get customer details ${req.params['phoneNumber']}`);
      const phoneNumber: any = req.params.phoneNumber;
      const requestBody = req.body;

      if (ObjectUtil.isNullOrUndefinedOrEmpty(phoneNumber)) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      const db = PrismaConnectionService.getDBConnection();

      const customerDetails = await db.customer.findFirst({
        where: {
          phoneNumber,
          vendorId: requestBody.headers.vendorId,
        },
        select: {
          id: true,
          name: true,
          phoneNumber: true,
          customerAddress: {
            select: {
              id: true,
              name: true,
              address: true,
              landmark: true,
              pincode: true,
              state: true,
              city: true,
              country: true,
              phoneNumber: true,
              lat: true,
              long: true,
              status: true,
              createdAt: true,
            },
          },
        },
      });

      res.status(CommonConstants.SUCCESS_CODE.OK).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.fetchRecordsSuccess,
        data: customerDetails,
      });
    } catch (error) {
      console.error(error);
      logger.error(`Got error while inserting new vendor -> ${error.message}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }

  async createCustomer(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info(`About to insert new customer ${JSON.stringify(req.body)}`);
      const requestBody = req.body;

      if (
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.customerName) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.name) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.address) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.pincode) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.city) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.customerPhoneNumber) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.phoneNumber) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.country) ||
        ObjectUtil.isNullOrUndefinedOrEmpty(requestBody.state)
      ) {
        res.status(CommonConstants.ERROR_CODE.BAD_REQUEST).send({
          status: CommonConstants.ERROR,
          message: StatusMessages.parameterMission,
        });
        return next();
      }

      const db = PrismaConnectionService.getDBConnection();

      let request: Prisma.customerAddressCreateInput = {
        name: requestBody.name,
        address: requestBody.address,
        landmark: requestBody.landmark || '',
        pincode: requestBody.pincode,
        state: requestBody.state,
        city: requestBody.city,
        country: requestBody.country,
        phoneNumber: requestBody.phoneNumber,
        lat: requestBody.lat || '',
        long: requestBody.long || '',
        customer: {
          connectOrCreate: {
            where: {
              phoneNumber: requestBody.customerPhoneNumber,
            },
            create: {
              name: requestBody.customerName,
              phoneNumber: requestBody.customerPhoneNumber,
              vendor: {
                connect: {
                  id: requestBody.headers.vendorId,
                },
              },
            },
          },
        },
      };

      await db.customerAddress.updateMany({
        where: {
          phoneNumber: requestBody.phoneNumber,
        },
        data: {
          status: 'IN_ACTIVE',
        },
      });

      const customerDetails = await db.customerAddress.create({
        data: request,
        select: {
          id: true,
          name: true,
          address: true,
          landmark: true,
          pincode: true,
          state: true,
          city: true,
          country: true,
          phoneNumber: true,
          lat: true,
          long: true,
        },
      });

      res.status(CommonConstants.SUCCESS_CODE.CREATED).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.newCustomerCreated,
        data: customerDetails,
      });
    } catch (error) {
      console.error(error);
      logger.error(
        `Got error while inserting new customer -> ${error.message}`
      );
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }

  async getCustomers(req: Request, res: Response, next: NextFunction) {
    try {
      logger.info(`About to get users`);
      let sortOrder = { id: 'asc' };

      const requestBody = req.body;
      const { pageNo, pageSize, search } = requestBody;
      const { limit, offset } = getPagination(+pageNo, +pageSize);
      if (requestBody.sortOrder) {
        sortOrder = requestBody.sortOrder;
      }
      const db = PrismaConnectionService.getDBConnection();

      const searchableFields = [];
      const searchQuery = [];

      if (search) {
        searchableFields.forEach((field) => {
          searchQuery.push({
            [field]: { contains: search, mode: 'insensitive' },
          });
        });
      }

      // let as: Prisma.customerFindManyArgs = {
      //   select: {
      //     name: true,
      //     phoneNumber: true,
      //     createdAt: true,
      //     _count: {
      //       select: {
      //         customerAddress: true,
      //       },
      //     },
      //   },
      // };

      let query: any = {
        skip: offset,
        take: limit,
        orderBy: sortOrder as any,
        where: { vendorId: requestBody.headers.vendorId },
      };

      query.select = {
        name: true,
        phoneNumber: true,
        createdAt: true,
        _count: {
          select: {
            customerAddress: true,
            orderDeliveryAddress: true,
          },
        },
      };

      const actualUsers = await db.customer.findMany(query);
      // const users = [];
      // actualUsers.forEach(async(actualUser) => {
      //   let user: any = actualUser;
      //   // @ts-ignore
      //   user.noOfAddress = user._count.customerAddress;
      //   user.noOfOrders = user._count.customerAddress;
      //   // @ts-ignore
      //   delete user._count;

      //   await db.order.count()

      //   users.push(user);
      // });

      let countQuery = query;
      delete countQuery.skip;
      delete countQuery.select;
      delete countQuery.take;

      const usersCount = await db.customer.count(query);

      res.status(CommonConstants.SUCCESS_CODE.OK).send({
        status: CommonConstants.SUCCESS,
        message: StatusMessages.getUsers,
        data: { records: actualUsers, totalCount: usersCount },
      });
    } catch (error) {
      console.error(error);
      logger.error(`Got error while inserting new vendor -> ${error.message}`);
      res.status(CommonConstants.ERROR_CODE.INTERNAL_SERVER_ERROR).send({
        status: CommonConstants.ERROR,
        message: StatusMessages.wentWrong,
      });
    }
  }
}
