import express = require('express');
import cors = require('cors');
import bodyParser = require('body-parser');
import config = require('config');
import LoggerFactory from './helpers/LoggerFactory';
import EurekaHelperService from './service/EurekaHelperService';
import { PrismaClient } from '@prisma/client';
import { ErrorHandler } from './middleware/ErrorHandler';
import { requestMiddleware, responseMiddleware } from './middleware';

require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` });

const baseRouter = require('./router/baseRouter');
const vendorRouter = require('./router/vendorRouter');
const userRouter = require('./router/userRouter');
const MediaUploadController = require('./controller/MediaUploadController');
const branchRouter = require('./router/branchRouter');
const orderRouter = require('./router/orderRouter');
const customerRouter = require('./router/customerRouter');

const server = express();

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));
server.use(cors());
server.options('*', cors());

const logger = LoggerFactory.getLogger(module);

const port = config.get('SERVER.port');
const appPrefixURL: string = config.get('SERVER.appPrefixURL');
const prisma = new PrismaClient({
  log: [
    {
      emit: 'event',
      level: 'query',
    },
  ],
});

prisma.$on('query', (e) => {
  console.log('Query: ' + e.query);
  console.log('Params: ' + e.params);
  console.log('Duration: ' + e.duration + 'ms');
});

server.use(requestMiddleware);
server.use(appPrefixURL, baseRouter);
server.use(appPrefixURL, customerRouter);
server.use(appPrefixURL, vendorRouter);
server.use(appPrefixURL, MediaUploadController);
server.use(appPrefixURL, userRouter);
server.use(appPrefixURL, branchRouter);
server.use(appPrefixURL, orderRouter);
server.use(ErrorHandler.handle);
server.use(responseMiddleware);

server.use(express.static('public'));

server.listen(port, () => {
  logger.debug(`${new Date()} Server listening on port ${port}!}`);
});

if (config.get('EUREKA.eurekaRequired')) {
  EurekaHelperService.registerWithEureka();
}
