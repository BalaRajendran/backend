FROM node:16-alpine

# Create Directory for the Container
WORKDIR /usr/src/delimile-delivery-service

COPY package*.json ./

# generated prisma files
COPY prisma ./prisma/

# COPY tsconfig.json file
COPY tsconfig.json ./

# COPY
COPY . .

# RUN npm i -g @nestjs/cli
RUN ls -l
# Install all Packages
RUN npm install

RUN npm run prisma:migrate

RUN npm run build

RUN ls -l

# Start
CMD [ "npm","run","start" ]
EXPOSE 9001
