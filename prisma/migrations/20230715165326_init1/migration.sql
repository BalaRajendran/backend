-- DropForeignKey
ALTER TABLE "track_order" DROP CONSTRAINT "track_order_branchId_fkey";

-- AlterTable
ALTER TABLE "track_order" ALTER COLUMN "branchId" DROP NOT NULL;

-- AddForeignKey
ALTER TABLE "track_order" ADD CONSTRAINT "track_order_branchId_fkey" FOREIGN KEY ("branchId") REFERENCES "branch"("id") ON DELETE SET NULL ON UPDATE CASCADE;
