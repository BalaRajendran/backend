/*
  Warnings:

  - You are about to drop the column `deliveryReason` on the `orderDeliveryDetails` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "orderDeliveryDetails" DROP COLUMN "deliveryReason",
ADD COLUMN     "cancelledOrReturnReason" TEXT;
