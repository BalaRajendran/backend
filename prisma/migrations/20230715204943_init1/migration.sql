/*
  Warnings:

  - The `deliveryPickUpTime` column on the `orderDeliveryDetails` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - The `deliveredTime` column on the `orderDeliveryDetails` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- AlterTable
ALTER TABLE "orderDeliveryDetails" DROP COLUMN "deliveryPickUpTime",
ADD COLUMN     "deliveryPickUpTime" TIMESTAMP(3),
DROP COLUMN "deliveredTime",
ADD COLUMN     "deliveredTime" TIMESTAMP(3);
