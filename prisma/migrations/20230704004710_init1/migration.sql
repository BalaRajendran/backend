-- CreateEnum
CREATE TYPE "orderStatus" AS ENUM ('CREATED', 'ASSIGNED_DE', 'DE_ACCEPTED', 'OUT_FOR_DELIVERY', 'DELIVERED', 'RETURNED', 'CANCELED', 'DAMAGED', 'DELETED');

-- CreateEnum
CREATE TYPE "userAccessType" AS ENUM ('ALL', 'INDIVIDUAL');

-- CreateEnum
CREATE TYPE "status" AS ENUM ('ACTIVE', 'IN_ACTIVE', 'DELETED');

-- CreateEnum
CREATE TYPE "orderType" AS ENUM ('DELIMILE', 'API_SYNC');

-- CreateEnum
CREATE TYPE "validationStatus" AS ENUM ('VERIFIED', 'NOT_VERIFIED');

-- CreateEnum
CREATE TYPE "paymentMode" AS ENUM ('CASH_ON_DELIVERY', 'PAID_ONLINE', 'UPI');

-- CreateTable
CREATE TABLE "vendor" (
    "id" TEXT NOT NULL,
    "businessName" TEXT NOT NULL,
    "businessType" INTEGER NOT NULL,
    "email" TEXT NOT NULL,
    "phoneNumber" TEXT NOT NULL,
    "phoneNumberStatus" "validationStatus" NOT NULL DEFAULT 'NOT_VERIFIED',
    "avatar" TEXT,
    "emailStatus" "validationStatus" NOT NULL DEFAULT 'NOT_VERIFIED',
    "gstNo" TEXT,
    "lat" TEXT,
    "long" TEXT,
    "status" "status" NOT NULL DEFAULT 'ACTIVE',
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "createdBy" TEXT NOT NULL DEFAULT 'ADMIN',
    "updatedBy" TEXT NOT NULL DEFAULT 'ADMIN',

    CONSTRAINT "vendor_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "branch" (
    "id" SERIAL NOT NULL,
    "vendorId" TEXT NOT NULL,
    "branchName" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "emailStatus" "validationStatus" NOT NULL DEFAULT 'NOT_VERIFIED',
    "address1" TEXT NOT NULL,
    "branchType" INTEGER NOT NULL,
    "avatar" TEXT,
    "address2" TEXT,
    "landmark" TEXT,
    "city" TEXT NOT NULL,
    "state" TEXT NOT NULL,
    "country" TEXT NOT NULL,
    "pincode" TEXT NOT NULL,
    "phoneNumber1" TEXT NOT NULL,
    "phoneNumber2" TEXT,
    "phoneNumber1Status" "validationStatus" NOT NULL DEFAULT 'NOT_VERIFIED',
    "phoneNumber2Status" "validationStatus" NOT NULL DEFAULT 'NOT_VERIFIED',
    "status" "status" NOT NULL DEFAULT 'ACTIVE',
    "startsFrom" INTEGER NOT NULL,
    "closeOn" INTEGER NOT NULL,
    "pickupStartsFrom" INTEGER NOT NULL,
    "pickupCloseAt" INTEGER NOT NULL,
    "weekOffDays" BOOLEAN[],
    "lat" TEXT NOT NULL,
    "long" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "createdBy" TEXT NOT NULL DEFAULT 'ADMIN',
    "updatedBy" TEXT NOT NULL DEFAULT 'ADMIN',

    CONSTRAINT "branch_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "role" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT,
    "group" TEXT NOT NULL,
    "status" "status" NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "createdBy" TEXT NOT NULL DEFAULT 'ADMIN',
    "updatedBy" TEXT NOT NULL DEFAULT 'ADMIN',

    CONSTRAINT "role_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "user" (
    "id" SERIAL NOT NULL,
    "vendorId" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "emailStatus" "validationStatus" NOT NULL DEFAULT 'NOT_VERIFIED',
    "phoneNumberStatus" "validationStatus" NOT NULL DEFAULT 'NOT_VERIFIED',
    "phoneNumber" TEXT NOT NULL,
    "roleId" INTEGER NOT NULL,
    "accessType" "userAccessType" NOT NULL DEFAULT 'INDIVIDUAL',
    "password" TEXT NOT NULL,
    "lastLoginTime" TIMESTAMP(3) NOT NULL,
    "status" "status" NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "createdBy" TEXT NOT NULL DEFAULT 'ADMIN',
    "updatedBy" TEXT NOT NULL DEFAULT 'ADMIN',
    "branchId" INTEGER,

    CONSTRAINT "user_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "assignedUserBranch" (
    "id" SERIAL NOT NULL,
    "userId" INTEGER NOT NULL,
    "branchId" INTEGER NOT NULL,

    CONSTRAINT "assignedUserBranch_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "customer" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "phoneNumber" TEXT NOT NULL,
    "vendorId" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "createdBy" TEXT NOT NULL DEFAULT 'ADMIN',
    "updatedBy" TEXT NOT NULL DEFAULT 'ADMIN',

    CONSTRAINT "customer_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "customerAddress" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "address" TEXT NOT NULL,
    "landmark" TEXT,
    "pincode" TEXT NOT NULL,
    "state" TEXT NOT NULL,
    "city" TEXT NOT NULL,
    "country" TEXT NOT NULL,
    "phoneNumber" TEXT NOT NULL,
    "lat" TEXT,
    "long" TEXT,
    "status" "status" NOT NULL DEFAULT 'ACTIVE',
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "createdBy" TEXT NOT NULL DEFAULT 'ADMIN',
    "updatedBy" TEXT NOT NULL DEFAULT 'ADMIN',
    "customerId" INTEGER,

    CONSTRAINT "customerAddress_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "order" (
    "id" SERIAL NOT NULL,
    "vendorId" TEXT NOT NULL,
    "branchId" INTEGER NOT NULL,
    "referenceOrderNumber" TEXT NOT NULL,
    "orderNumber" TEXT NOT NULL,
    "discountPercent" INTEGER NOT NULL,
    "discountAmount" DOUBLE PRECISION NOT NULL,
    "totalAmount" DOUBLE PRECISION NOT NULL,
    "gstPercent" INTEGER NOT NULL,
    "netAmount" DOUBLE PRECISION NOT NULL,
    "paymentMode" "paymentMode" NOT NULL,
    "deliverySlotFrom" INTEGER NOT NULL,
    "deliverySlotTo" INTEGER NOT NULL,
    "orderType" "orderType" NOT NULL DEFAULT 'DELIMILE',
    "deliveryDate" TIMESTAMP(3) NOT NULL,
    "status" "orderStatus" NOT NULL DEFAULT 'CREATED',
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "createdBy" TEXT NOT NULL DEFAULT 'ADMIN',
    "updatedBy" TEXT NOT NULL DEFAULT 'ADMIN',

    CONSTRAINT "order_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "assignedOrderDetailsToDE" (
    "id" SERIAL NOT NULL,
    "userId" INTEGER,
    "orderId" INTEGER,
    "deliveryDate" TIMESTAMP(3) NOT NULL,
    "fromTime" INTEGER NOT NULL,
    "toTime" INTEGER NOT NULL,

    CONSTRAINT "assignedOrderDetailsToDE_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "orderDeliveryDetails" (
    "id" SERIAL NOT NULL,
    "orderId" INTEGER,
    "deliveryExecutiveId" INTEGER,
    "deliveryFeedback" TEXT,
    "deliveryFeedbackRating" TEXT,
    "deliveryPickUpTime" TEXT,
    "deliveredTime" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "createdBy" TEXT NOT NULL DEFAULT 'ADMIN',
    "updatedBy" TEXT NOT NULL DEFAULT 'ADMIN',

    CONSTRAINT "orderDeliveryDetails_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "orderDeliveryAddress" (
    "id" SERIAL NOT NULL,
    "orderId" INTEGER NOT NULL,
    "vendorId" TEXT NOT NULL,
    "branchId" INTEGER NOT NULL,
    "customerId" INTEGER NOT NULL,
    "name" TEXT NOT NULL,
    "address" TEXT NOT NULL,
    "landmark" TEXT,
    "pincode" TEXT NOT NULL,
    "state" TEXT NOT NULL,
    "city" TEXT NOT NULL,
    "country" TEXT NOT NULL,
    "phoneNumber" TEXT NOT NULL,
    "lat" TEXT,
    "long" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "createdBy" TEXT NOT NULL DEFAULT 'ADMIN',
    "updatedBy" TEXT NOT NULL DEFAULT 'ADMIN',

    CONSTRAINT "orderDeliveryAddress_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "orderItem" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "quantity" INTEGER NOT NULL,
    "unit" DOUBLE PRECISION NOT NULL,
    "vendorId" TEXT NOT NULL,
    "branchId" INTEGER NOT NULL,
    "orderId" INTEGER NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "createdBy" TEXT NOT NULL DEFAULT 'ADMIN',
    "updatedBy" TEXT NOT NULL DEFAULT 'ADMIN',

    CONSTRAINT "orderItem_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "track_order" (
    "id" SERIAL NOT NULL,
    "status" "orderStatus" NOT NULL,
    "remarks" TEXT NOT NULL,
    "vendorId" TEXT NOT NULL,
    "branchId" INTEGER NOT NULL,
    "orderId" INTEGER NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "createdBy" TEXT NOT NULL DEFAULT 'ADMIN',

    CONSTRAINT "track_order_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "pincode" (
    "id" SERIAL NOT NULL,
    "pincode" TEXT,
    "area" TEXT,
    "city" TEXT,
    "district" TEXT,
    "state" TEXT,
    "country" TEXT,

    CONSTRAINT "pincode_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "vendor_email_key" ON "vendor"("email");

-- CreateIndex
CREATE UNIQUE INDEX "vendor_phoneNumber_key" ON "vendor"("phoneNumber");

-- CreateIndex
CREATE UNIQUE INDEX "branch_email_key" ON "branch"("email");

-- CreateIndex
CREATE UNIQUE INDEX "branch_phoneNumber1_key" ON "branch"("phoneNumber1");

-- CreateIndex
CREATE UNIQUE INDEX "role_name_key" ON "role"("name");

-- CreateIndex
CREATE UNIQUE INDEX "customer_phoneNumber_key" ON "customer"("phoneNumber");

-- CreateIndex
CREATE UNIQUE INDEX "assignedOrderDetailsToDE_orderId_key" ON "assignedOrderDetailsToDE"("orderId");

-- CreateIndex
CREATE UNIQUE INDEX "orderDeliveryDetails_orderId_key" ON "orderDeliveryDetails"("orderId");

-- AddForeignKey
ALTER TABLE "branch" ADD CONSTRAINT "branch_vendorId_fkey" FOREIGN KEY ("vendorId") REFERENCES "vendor"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "user" ADD CONSTRAINT "user_vendorId_fkey" FOREIGN KEY ("vendorId") REFERENCES "vendor"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "user" ADD CONSTRAINT "user_roleId_fkey" FOREIGN KEY ("roleId") REFERENCES "role"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "user" ADD CONSTRAINT "user_branchId_fkey" FOREIGN KEY ("branchId") REFERENCES "branch"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "assignedUserBranch" ADD CONSTRAINT "assignedUserBranch_userId_fkey" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "assignedUserBranch" ADD CONSTRAINT "assignedUserBranch_branchId_fkey" FOREIGN KEY ("branchId") REFERENCES "branch"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "customer" ADD CONSTRAINT "customer_vendorId_fkey" FOREIGN KEY ("vendorId") REFERENCES "vendor"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "customerAddress" ADD CONSTRAINT "customerAddress_customerId_fkey" FOREIGN KEY ("customerId") REFERENCES "customer"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "order" ADD CONSTRAINT "order_vendorId_fkey" FOREIGN KEY ("vendorId") REFERENCES "vendor"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "order" ADD CONSTRAINT "order_branchId_fkey" FOREIGN KEY ("branchId") REFERENCES "branch"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "assignedOrderDetailsToDE" ADD CONSTRAINT "assignedOrderDetailsToDE_userId_fkey" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "assignedOrderDetailsToDE" ADD CONSTRAINT "assignedOrderDetailsToDE_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES "order"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "orderDeliveryDetails" ADD CONSTRAINT "orderDeliveryDetails_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES "order"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "orderDeliveryDetails" ADD CONSTRAINT "orderDeliveryDetails_deliveryExecutiveId_fkey" FOREIGN KEY ("deliveryExecutiveId") REFERENCES "user"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "orderDeliveryAddress" ADD CONSTRAINT "orderDeliveryAddress_vendorId_fkey" FOREIGN KEY ("vendorId") REFERENCES "vendor"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "orderDeliveryAddress" ADD CONSTRAINT "orderDeliveryAddress_branchId_fkey" FOREIGN KEY ("branchId") REFERENCES "branch"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "orderDeliveryAddress" ADD CONSTRAINT "orderDeliveryAddress_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES "order"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "orderDeliveryAddress" ADD CONSTRAINT "orderDeliveryAddress_customerId_fkey" FOREIGN KEY ("customerId") REFERENCES "customer"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "orderItem" ADD CONSTRAINT "orderItem_vendorId_fkey" FOREIGN KEY ("vendorId") REFERENCES "vendor"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "orderItem" ADD CONSTRAINT "orderItem_branchId_fkey" FOREIGN KEY ("branchId") REFERENCES "branch"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "orderItem" ADD CONSTRAINT "orderItem_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES "order"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "track_order" ADD CONSTRAINT "track_order_vendorId_fkey" FOREIGN KEY ("vendorId") REFERENCES "vendor"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "track_order" ADD CONSTRAINT "track_order_branchId_fkey" FOREIGN KEY ("branchId") REFERENCES "branch"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "track_order" ADD CONSTRAINT "track_order_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES "order"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
