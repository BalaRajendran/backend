-- AlterTable
ALTER TABLE "order" ADD COLUMN     "collectionAmount" INTEGER NOT NULL DEFAULT 0,
ALTER COLUMN "discountPercent" SET DEFAULT 0,
ALTER COLUMN "discountAmount" SET DEFAULT 0,
ALTER COLUMN "totalAmount" SET DEFAULT 0,
ALTER COLUMN "gstPercent" SET DEFAULT 0;

-- CreateTable
CREATE TABLE "deliveryUserMetadata" (
    "id" SERIAL NOT NULL,
    "userId" INTEGER NOT NULL,
    "noOfSuccessfullDeliveries" INTEGER NOT NULL DEFAULT 0,
    "noOfCanceledDeliveries" INTEGER NOT NULL DEFAULT 0,
    "averageRating" INTEGER NOT NULL DEFAULT 0,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "deliveryUserMetadata_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "deliveryUserMetadata" ADD CONSTRAINT "deliveryUserMetadata_userId_fkey" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
