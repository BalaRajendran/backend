/*
  Warnings:

  - You are about to drop the column `collectionAmount` on the `order` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "order" DROP COLUMN "collectionAmount";

-- AlterTable
ALTER TABLE "orderDeliveryDetails" ADD COLUMN     "collectionAmount" INTEGER NOT NULL DEFAULT 0;
