/*
  Warnings:

  - A unique constraint covering the columns `[userId]` on the table `deliveryUserMetadata` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "deliveryUserMetadata_userId_key" ON "deliveryUserMetadata"("userId");
